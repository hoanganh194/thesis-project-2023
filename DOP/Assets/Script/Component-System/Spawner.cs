using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.UI;
using static UnityEditor.PlayerSettings;

namespace PureECS
{
    public struct SpawnerComponent : IComponentData
    {
        public int number;
        public Entity prefab1;
        public Entity prefab2;

        public int teamOne;
        public int teamTwo;

    }

    public partial struct TankRespawnSystem : ISystem
    {
        uint updateCounter;
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            // This call makes the system not update unless at least one entity in the world exists that has the Spawner component.
            state.RequireForUpdate<SpawnerComponent>();
            state.RequireForUpdate<TankHitpointComponent>();
        }

 
        public void OnUpdate(ref SystemState state)
        {
            var spawner = SystemAPI.GetSingleton<SpawnerComponent>();
            var ecbSingleton = SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>();
            var ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);
     
            int teamOne= 0;
            int teamTwo = 0;
            foreach (var (hitpoint, entity) in SystemAPI.Query<RefRW<TankHitpointComponent>>().WithEntityAccess())
            {
                if (hitpoint.ValueRW.isDamaged)
                {
                    hitpoint.ValueRW.isDamaged = false;
                    hitpoint.ValueRW.hp -= hitpoint.ValueRO.damage;
                }
                if(hitpoint.ValueRO.hp <= 0)
                {
                    ecb.DestroyEntity(entity);
                    switch (hitpoint.ValueRO.team)
                    {
                        case 0:
                            teamOne++;
                            break;
                        case 1:
                            teamTwo++;
                            break;
                    }
                }   
            }
            foreach(var co in SystemAPI.Query<RefRW<SpawnerComponent>>())
            {
                co.ValueRW.teamOne = teamOne;
                co.ValueRW.teamTwo = teamTwo;
            }

            //spawner.teamOne = teamOne;
            //spawner.teamTwo = teamTwo;

        }
    }


    public partial struct SpawnSystem : ISystem
    {
        uint updateCounter;

        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            // This call makes the system not update unless at least one entity in the world exists that has the Spawner component.
            state.RequireForUpdate<SpawnerComponent>();
            updateCounter = 0;
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var spawner = SystemAPI.GetSingleton<SpawnerComponent>();
            if (updateCounter < spawner.number)
            {
                CreateTank(ref state, spawner.prefab1, new float3(0, 0, 0), 0);
                CreateTank(ref state, spawner.prefab2, new float3(0, 0, 0), 1);
            }

            for(int i = 0; i < spawner.teamOne; i++)
            {
                CreateTank(ref state, spawner.prefab1, new float3(0, 0, 0), 0);
            }
            for (int i = 0; i < spawner.teamTwo; i++)
            {
                CreateTank(ref state, spawner.prefab2, new float3(0, 0, 0), 1);
            }
            foreach (var co in SystemAPI.Query<RefRW<SpawnerComponent>>())
            {
                co.ValueRW.teamOne = 0;
                co.ValueRW.teamTwo = 0;
            }

        }
        [BurstCompile]
        private void CreateTank(ref SystemState state, Entity prefab, float3 position, int team)
        {
            updateCounter++;
            var entity = state.EntityManager.Instantiate(prefab);
            var transform = SystemAPI.GetComponentRW<LocalTransform>(entity);
            var random = Unity.Mathematics.Random.CreateFromIndex(updateCounter);
            var targetPosition = new float3(random.NextFloat(-0.5f, 0.5f), random.NextFloat(-0.5f, 0.5f), 0);
            transform.ValueRW.Position = targetPosition;
            var tank = SystemAPI.GetComponentRW<TankHitpointComponent>(entity);
            tank.ValueRW.team = team;
            
        }               
    }


}
