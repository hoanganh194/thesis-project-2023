using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Transforms;
using TMPro;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
namespace PureECS
{
    public struct TankBulletComponent : IComponentData
    {
        public float damage;
        public int team;

        public Entity target;

        public bool destroyBullet;
    }


    public partial struct TankBulletSystem : ISystem
    {
        private EntityQuery otherTanksQuery;
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<TankBulletComponent>();
            otherTanksQuery = new EntityQueryBuilder(Allocator.Temp)
              .WithAllRW<TankHitpointComponent>()
              .WithAllRW<LocalTransform>()
              .Build(ref state);

        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecbSingleton = SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>();
            var ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);

            foreach (var (bullet, tranform, entity) in SystemAPI.Query<RefRW<TankBulletComponent>, RefRW<LocalTransform>>().WithEntityAccess())
            {
                if (!state.EntityManager.Exists(bullet.ValueRO.target))
                {
                    ecb.DestroyEntity(entity);
                    continue;
                }
                if(SystemAPI.HasComponent<LocalToWorld>(bullet.ValueRO.target) && SystemAPI.HasComponent<TankHitpointComponent>(bullet.ValueRO.target))
                {
                    var targetPosition = SystemAPI.GetComponentRW<LocalToWorld>(bullet.ValueRO.target).ValueRO;

                    if (Vector3.Distance(tranform.ValueRW.Position, targetPosition.Position) < 0.1f)
                    {
                        var hp = SystemAPI.GetComponentRW<TankHitpointComponent>(bullet.ValueRO.target);
                        hp.ValueRW.damage = bullet.ValueRO.damage;
                        hp.ValueRW.isDamaged = true;
                        ecb.DestroyEntity(entity);
                        continue;
                    }
                    tranform.ValueRW.Position = Vector3.MoveTowards(tranform.ValueRW.Position, targetPosition.Position, 2 * SystemAPI.Time.DeltaTime);

                    var a = new float3(0, 1, 0);
                    var b = math.normalize(targetPosition.Position - tranform.ValueRO.Position);
                    var o = new float3(0, 0, 0);
                    var angle = math.acos(math.dot(a, b) / (math.distance(a, o) * math.distance(b, o)));

                    if (b.x > 0)
                    {
                        angle = -angle;
                    }
                    tranform.ValueRW.Rotation = quaternion.Euler(0, 0, angle);
                }
             


               
            }
        }
    }
}


