using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Transforms;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;


namespace PureECS
{

    public struct TankGunComponent : IComponentData
    {
        public bool init;
        public float damage;
        public int team;
        public float delayPerShot;
        public float delay;

        public Entity bullet;
        public bool spawn;
        public Entity target;
        public float3 targetPosition;

    }

    public partial struct TankGunFindTargetSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
           state.RequireForUpdate<TankGunComponent>();
        }
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            foreach (var (gun, worldTransform) in SystemAPI.Query<RefRW<TankGunComponent>, RefRO<LocalToWorld>>())
            {
                Entity target = default;
                float minDistance = float.MaxValue;
                float3 basePosition = worldTransform.ValueRO.Position;
                float3 targetPosition = float3.zero;

                foreach (var (tankHp, enemyTransform, entityTank) in SystemAPI.Query<RefRO<TankHitpointComponent>, RefRW<LocalTransform>>().WithEntityAccess())
                {
                    if (tankHp.ValueRO.team != gun.ValueRW.team)
                    {
                        var targetDistance = math.distancesq(basePosition, enemyTransform.ValueRO.Position);
                        if (targetDistance < minDistance)
                        {
                            minDistance = targetDistance;
                            target = entityTank;
                            targetPosition = enemyTransform.ValueRO.Position;
                        }
                    }
                }
                gun.ValueRW.target = target;
                gun.ValueRW.targetPosition = targetPosition;
            }
        }
    }

    public partial struct TankGunRotateSystem: ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<TankGunComponent>();
        }
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            foreach (var (gun, worldTransform, localTransform, parent) in SystemAPI.Query<RefRW<TankGunComponent>, RefRO<LocalToWorld>, RefRW<LocalTransform>, RefRO<Parent>>())
            {
                

                var a = new float3(0, 1, 0);
                var b = math.normalize(gun.ValueRW.targetPosition - worldTransform.ValueRO.Position);
                var o = new float3(0, 0, 0);
                var angle = math.acos(math.dot(a, b) / (math.distance(a, o) * math.distance(b, o)));

                if (b.x > 0)
                {
                    angle = -angle;
                }

                var parentInverse = Quaternion.Inverse(state.EntityManager.GetComponentData<LocalTransform>(parent.ValueRO.Value).Rotation);
                var childQuaternion = quaternion.Euler(0, 0, angle);


                var localQuaternion = parentInverse * childQuaternion;

              
                localTransform.ValueRW.Rotation = localQuaternion;
            }
        }
    }

   

    public partial struct TankBulletSpawnSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<TankGunComponent>();
        }
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {

            foreach (var (gun, worldTransform, entity) in SystemAPI.Query<RefRW<TankGunComponent>, RefRO<LocalToWorld>>().WithEntityAccess())
            {
                if (gun.ValueRW.delay > 0)
                {
                    gun.ValueRW.delay -= SystemAPI.Time.DeltaTime;
                    continue;
                }

                gun.ValueRW.delay = gun.ValueRW.delayPerShot;
              

                if (state.EntityManager.Exists(gun.ValueRO.target) && state.EntityManager.Exists(entity))
                {
                    var bulletEntity = state.EntityManager.Instantiate(gun.ValueRO.bullet);
                    var bulletTransform = SystemAPI.GetComponentRW<LocalTransform>(bulletEntity);
                    bulletTransform.ValueRW.Position = worldTransform.ValueRO.Position;
                    var bulletComponent = SystemAPI.GetComponentRW<TankBulletComponent>(bulletEntity);
                    bulletComponent.ValueRW.team = gun.ValueRW.team;
                    bulletComponent.ValueRW.damage = gun.ValueRW.damage;
                    bulletComponent.ValueRW.target = gun.ValueRW.target;

                }

            }
        }



    }

    
}


