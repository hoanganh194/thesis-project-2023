using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Transforms;
using static UnityEngine.EventSystems.EventTrigger;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Collections;
namespace PureECS
{
    public struct TankMoveComponent : IComponentData
    {
        public float speed;
        public bool moving;
        public float3 targetPosition;

    }

    public partial struct TankMoveSystem : ISystem
    {
        uint updateCounter;
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            // This call makes the system not update unless at least one entity in the world exists that has the Spawner component.
            state.RequireForUpdate<TankMoveComponent>();
        }
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            float deltaTime = SystemAPI.Time.DeltaTime;
            var bound = new float4(-2, -4, 2, 4);
            foreach (var (tank, tranform) in SystemAPI.Query<RefRW<TankMoveComponent>, RefRW<LocalTransform>>())
            {

                if (tank.ValueRW.moving)
                {
                    //Debug.DrawLine(tranform.ValueRO.Position, tank.ValueRW.targetPosition, Color.red, SystemAPI.Time.DeltaTime);
                    tranform.ValueRW.Position = Vector3.MoveTowards(tranform.ValueRW.Position, tank.ValueRW.targetPosition, tank.ValueRO.speed * deltaTime);
                    if (tranform.ValueRO.Position.x == tank.ValueRW.targetPosition.x && tranform.ValueRO.Position.y == tank.ValueRW.targetPosition.y)
                    {
                        tank.ValueRW.moving = false;
                    }
                    else
                    {
                        continue;
                    }

                }

                var random = Unity.Mathematics.Random.CreateFromIndex(++updateCounter);

                var index = random.NextInt(0, 4);
                float pos;
                var targetPosition = new float3();

                switch (index)
                {
                    case 0:
                        pos = random.NextFloat(-bound.x, bound.x);
                        targetPosition = new float3(pos, bound.y, 0);
                        break;
                    case 1:
                        pos = random.NextFloat(-bound.x, bound.x);
                        targetPosition = new float3(pos, bound.w, 0);
                        break;
                    case 2:
                        pos = random.NextFloat(-bound.y, bound.y);
                        targetPosition = new float3(bound.x, pos, 0);
                        break;
                    case 3:
                        pos = random.NextFloat(-bound.y, bound.y);
                        targetPosition = new float3(bound.z, pos, 0);
                        break;
                }
                tank.ValueRW.targetPosition = targetPosition;
                tank.ValueRW.moving = true;
                var a = new float3(0, 1, 0);
                var b = math.normalize(targetPosition - tranform.ValueRO.Position);
                var o = new float3(0, 0, 0);
                var angle = math.acos(math.dot(a, b) / (math.distance(a, o) * math.distance(b, o)));

                if (b.x > 0)
                {
                    angle = -angle;
                }
                tranform.ValueRW.Rotation = quaternion.Euler(0, 0, angle);


            }

        }
    }
}