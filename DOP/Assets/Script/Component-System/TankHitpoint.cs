using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Transforms;
namespace PureECS
{
    public struct TankHitpointComponent : IComponentData
    {
        public float hp;
        public int team;
        public bool isDamaged;
        public float damage;
    }

    public partial struct TankHitpointSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<TankHitpointComponent>();
        }
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecbSingleton = SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>();
            var ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);
            foreach (var (hitpoint, entity) in SystemAPI.Query<RefRW<TankHitpointComponent>>().WithEntityAccess())
            {
                if (hitpoint.ValueRO.hp <= 0)
                {
                    hitpoint.ValueRW.hp = 100;
                }
            }
        }
    }
}