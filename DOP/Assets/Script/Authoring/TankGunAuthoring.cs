using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Transforms;
namespace PureECS
{
    public class TankGunAuthoring : MonoBehaviour
    {
        public float damage;
        public int team;
        public GameObject bullet;
        public float delayPerShot;
        class Baker : Baker<TankGunAuthoring>
        {
            public override void Bake(TankGunAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent(entity, 
                    new TankGunComponent { damage = authoring.damage, 
                        team = authoring.team, 
                        delayPerShot = 0.1f,
                        bullet = GetEntity(authoring.bullet, TransformUsageFlags.Dynamic)
                    });

            }
        }
    }

}


