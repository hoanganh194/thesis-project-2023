using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


namespace PureECS
{

    public class SpawnerAuthoring : MonoBehaviour
    {
        public int number;
        public GameObject prefab1;
        public GameObject prefab2;

        class Baker : Baker<SpawnerAuthoring>
        {
            public override void Bake(SpawnerAuthoring spawner)
            {
                var entity = GetEntity(TransformUsageFlags.None);
                AddComponent(entity, new SpawnerComponent
                {
                    number = spawner.number,
                    prefab1 = GetEntity(spawner.prefab1, TransformUsageFlags.None),
                    prefab2 = GetEntity(spawner.prefab2, TransformUsageFlags.None),
                });
            }
        }


        private void Start()
        {
            var world = World.DefaultGameObjectInjectionWorld;
            var systemHandle = world.GetExistingSystem(typeof(SpawnSystem));

        }
    }
   

}
