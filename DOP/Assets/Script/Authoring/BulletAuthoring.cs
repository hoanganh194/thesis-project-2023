using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace PureECS
{
    public class BulletAuthoring : MonoBehaviour
    {
     
        class Baker : Baker<BulletAuthoring>
        {
            public override void Bake(BulletAuthoring spawner)
            {
                var entity = GetEntity(TransformUsageFlags.None);
                AddComponent(entity, new TankBulletComponent
                {

                });
            }
        }
    }
}
