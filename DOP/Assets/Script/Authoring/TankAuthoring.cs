using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

namespace PureECS
{
   
    public class TankAuthoring : MonoBehaviour
    {
        public SpriteRenderer spriteRenderer;
        public TankGunAuthoring gun;
        public int team;
        public float speed;
        public float hp;



        class Baker : Baker<TankAuthoring>
        {
            public override void Bake(TankAuthoring turret)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent(entity, new TankMoveComponent
                {
                    speed = turret.speed
                });
                AddComponent(entity, new TankHitpointComponent
                {
                    hp = turret.hp
                });
            }
        }

        
    }


}
