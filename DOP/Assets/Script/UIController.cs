using PureECS;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Scenes;
using UnityEngine;
using UnityEngine.UI;

namespace PureECS
{
    public class UIController : MonoBehaviour
    {
        public Text tankText;
        public Text bulletText;
        public Text frameText;

        EntityQuery tankQuery;
        EntityQuery bulletQuery;

        public float frameTime;
        private void Start()
        {
            var world = World.DefaultGameObjectInjectionWorld;
            tankQuery = world.EntityManager.CreateEntityQuery(typeof(TankMoveComponent));
            bulletQuery = world.EntityManager.CreateEntityQuery(typeof(TankBulletComponent));
            InvokeRepeating("UpdateFrame", 0, 1);
        }
        private void UpdateFrame()
        {
            tankText.text = "Tank: " + tankQuery.CalculateEntityCount().ToString();
            bulletText.text = "Bullet: " + bulletQuery.CalculateEntityCount().ToString();
            frameText.text = "FPS: " + (1.0f / Time.deltaTime).ToString("0.00");
        }

    }
}

