using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace PureOOP { 
    public class TankController : MonoBehaviour
    {
        public float hp;
        public int team;
        public float speed;

        private bool moving;
        private Vector3 targetPosition;
        private void Update()
        {
            if (hp <= 0)
            {
                Spawner.Instance.DetroyTank(gameObject, team);
                Spawner.Instance.CreateTank(team);
               
            }
            if (moving)
            {
                transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
                if(transform.position == targetPosition)
                {
                    moving = false;
                }
                else
                {
                    return;
                }
            }

            var bound = new Vector4(-2, -4, 2, 4);
            var random = new Unity.Mathematics.Random((uint)UnityEngine.Random.Range(1, 100000));
            var index = random.NextInt(0, 4);
            float pos;
            switch (index)
            {
                case 0:
                    pos = random.NextFloat(-bound.x, bound.x);
                    targetPosition = new Vector3(pos, bound.y, 0);
                    break;
                case 1:
                    pos = random.NextFloat(-bound.x, bound.x);
                    targetPosition = new Vector3(pos, bound.w, 0);
                    break;
                case 2:
                    pos = random.NextFloat(-bound.y, bound.y);
                    targetPosition = new Vector3(bound.x, pos, 0);
                    break;
                case 3:
                    pos = random.NextFloat(-bound.y, bound.y);
                    targetPosition = new Vector3(bound.z, pos, 0);
                    break;
                default:
                    pos = random.NextFloat(-bound.x, bound.x);
                    targetPosition = new Vector3(bound.x, pos, 0);
                    break;
            }
            var a = new float3(0, 1, 0);
            var b = math.normalize(targetPosition - transform.position);
            var o = new float3(0, 0, 0);
            var angle = math.acos(math.dot(a, b) / (math.distance(a, o) * math.distance(b, o)));

            if (b.x > 0)
            {
                angle = -angle;
            }
            moving = true;
            transform.rotation = quaternion.Euler(0, 0, angle);


        }

    }
}