using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace PureOOP { 
    public class TankBulletController : MonoBehaviour
    {
        public GameObject target;
        public float speed;
        public float damage;


        private void Update()
        {
            if(target == null)
            {
                Destroy(gameObject);
                return;
            }

            if(Vector3.Distance(transform.position, target.transform.position) < 0.1f)
            {
                var hp = target.GetComponent<TankController>();
                hp.hp -= damage;

                Destroy(gameObject);
                return;
            }       

            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);

            var a = new float3(0, 1, 0);
            var b = math.normalize(target.transform.position - transform.position);
            var o = new float3(0, 0, 0);
            var angle = math.acos(math.dot(a, b) / (math.distance(a, o) * math.distance(b, o)));

            if (b.x > 0)
            {
                angle = -angle;
            }
            transform.rotation = quaternion.Euler(0, 0, angle);
        }
    }

}