
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace PureOOP
{
    public class Spawner : MonoBehaviour
    {
        public static Spawner Instance;
        public GameObject prefab1;
        public GameObject prefab2;
        public int number;
        int updateCounter = 0;

        public List<GameObject> teamZeroTanks = new List<GameObject>();
        public List<GameObject> teamOneTanks = new List<GameObject>();


        private void Awake()
        {
            Application.targetFrameRate = 60;
            Instance = this;

        }


        private void Update()
        {
            if (updateCounter >= number)
            {
                return;
            }

            updateCounter += 2;
            CreateTank(prefab1, new float3(0, 0, 0), 0);
            CreateTank(prefab2, new float3(0, 0, 0), 1);
        }
        private void CreateTank(GameObject prefab, float3 position, int team)
        {
            var entity = Instantiate(prefab);
            position = new float3(UnityEngine.Random.Range(-0.5f, 0.5f), UnityEngine.Random.Range(-0.5f, 0.5f),0);

            entity.transform.position = position;
            var tank = entity.GetComponent<TankController>();
            tank.team = team;
            if (team == 0)
            {
                teamZeroTanks.Add(entity);
            }
            else
            {
                teamOneTanks.Add(entity);
            }

        }
        public void CreateTank(int team)
        {
            switch(team)
            {
                case 0:
                    CreateTank(prefab1, new float3(0, 0, 0), 0);
                    break;
                case 1:
                    CreateTank(prefab2, new float3(0, 0, 0), 1);
                    break;
            }
        }

        public void DetroyTank(GameObject tankController, int team)
        {
            if (team == 0)
            {
                teamZeroTanks.Remove(tankController);
            }
            else
            {
                teamOneTanks.Remove(tankController);
            }
            Destroy(tankController);
        }

    }
}

