using UnityEngine;
using UnityEngine.UI;

namespace PureOOP
{
    public class UIController : MonoBehaviour
    {
        public Text tankText;
        public Text bulletText;
        public Text frameText;

      

        public float frameTime;
        private void Start()
        {
            InvokeRepeating("UpdateFrame", 0, 1);
        }
        private void UpdateFrame()
        {
            tankText.text = "Tank: " + (Spawner.Instance.teamOneTanks.Count + Spawner.Instance.teamZeroTanks.Count);
            bulletText.text = "Bullet: " + (1);
            frameText.text = "FPS: " + (1.0f / Time.deltaTime).ToString("0.00");
        }
    }
}

