using PureOOP;
using Unity.Mathematics;
using UnityEngine;

namespace PureOOP { 
    public class TankGunController : MonoBehaviour
    {
        public int team;
        public GameObject bulletPrefab;
        public GameObject target;
        public float delayPerShot;

        public float delay;
        private void Awake()
        {
            delayPerShot = 0.1f;
        }

        private void Update()
        {
            if (delay > 0)
            {
                delay -= Time.deltaTime;
                return;
            }
            delay = delayPerShot;
            var targetList = team == 0 ? Spawner.Instance.teamOneTanks : Spawner.Instance.teamZeroTanks;

            float minDistance = float.MaxValue;
            target = null;
            for (int i = 0; i < targetList.Count; i++)
            {
                float distance = math.distance(transform.position, targetList[i].transform.position);
                if (distance < minDistance)
                { 
                    var tankController = targetList[i].GetComponent<TankController>();
                    minDistance = distance;
                    target = targetList[i];
                }
            }

            if (target != null)
            {
                var bullet = Instantiate(bulletPrefab);
                bullet.transform.position = transform.position;
                var bulletController = bullet.GetComponent<TankBulletController>();
                bulletController.target = target;
            }

        }

    }
}